/**
 * Copyright (C) 2002, 2003 Doug Bell <drbell@users.sourceforge.net>
 *
 * Some mixer routines from mplayer, http://mplayer.sourceforge.net.
 * Copyright (C) 2000-2002. by A'rpi/ESP-team & others
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef MIXER_H_INCLUDED
#define MIXER_H_INCLUDED

#include "tvtimeconf.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Sets the mixer device and channel.
 * All interfaces are scanned until one succeeds.
 */
void mixer_init( config_t *cfg, const char *v4ldev );
void mixer_exit( void );
void mixer_mute( int mute );

struct mixer {
/**
 * Sets the mixer device and channel.
 */
int (* set_device)( const char *devname, int verbose );

/**
 * Sets the initial state of the mixer device.
 */
void (* set_state)( int ismuted, int unmute_volume );

/**
 * Returns the current volume setting.
 */
int (* get_volume)( void );

/**
 * Returns the volume that would be used to restore the unmute state.
 */
int (* get_unmute_volume)( void );

/**
 * Tunes the relative volume.
 */
int (* set_volume)( int percentdiff );

/**
 * Sets the mute state.
 */
void (* mute)( int mute );

/**
 * Returns true if the mixer is muted.
 */
int (* ismute)( void );

/**
 * Closes the mixer device if it is open.
 */
void (* close_device)( void );
};

#pragma weak alsa_mixer
extern struct mixer alsa_mixer;
#pragma weak oss_mixer
extern struct mixer oss_mixer;
extern struct mixer *mixer;

#ifdef __cplusplus
};
#endif
#endif /* MIXER_H_INCLUDED */
